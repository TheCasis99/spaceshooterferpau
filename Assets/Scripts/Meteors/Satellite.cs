﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Satellite : MonoBehaviour
{

    protected bool launched;
    protected Vector2 moveSpeed;
    protected float rotationSpeed;
    public ParticleSystem particle;
    public AudioSource audioExplosion;

    private Transform graphics;
    private Collider2D SatelliteCollider;


    private Vector3 iniPos;

    void Start()
    {
        iniPos = transform.position;
        graphics = transform.GetChild(0);
        SatelliteCollider = GetComponent<Collider2D>();
    }

    public void LaunchSatellite(Vector3 position, Vector2 direction, float rotation)
    {
        transform.position = position;
        launched = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        moveSpeed = direction;
        rotationSpeed = rotation;
        graphics.gameObject.SetActive(true);
        SatelliteCollider.enabled = true;
    }

    // Update is called once per frame
    protected void Update()
    {
        if (launched)
        {
            transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
            graphics.Rotate(0, 0, rotationSpeed);
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            audioExplosion.Play();
            particle.Play();

        }
        else if (other.tag == "Finish")
        {
            Reset();
        }
    }

    protected void Reset()
    {
        graphics.gameObject.SetActive(false);
        SatelliteCollider.enabled = false;
        transform.position = iniPos;
        launched = false;
    }


}
