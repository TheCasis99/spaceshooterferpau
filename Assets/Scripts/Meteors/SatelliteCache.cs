﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteCache
{
    private Satellite[] satellites;
    private int currentSatellite = 0;


    public SatelliteCache(GameObject prefabSatellite, Vector3 position, Transform parent, int numSatellites)
    {
        satellites = new Satellite[numSatellites];

        Vector3 tmpPosition = position;
        for (int i = 0; i < numSatellites; i++)
        {
            satellites[i] = GameObject.Instantiate(prefabSatellite, tmpPosition, Quaternion.identity, parent).GetComponent<Satellite>();
            satellites[i].name = prefabSatellite.name + "_Satellite_" + i;
            tmpPosition.x += 1;
        }
    }

    public Satellite GetSatellite()
    {
        if (currentSatellite > satellites.Length - 1)
        {
            currentSatellite = 0;
        }

        return satellites[currentSatellite++];
    }
}
