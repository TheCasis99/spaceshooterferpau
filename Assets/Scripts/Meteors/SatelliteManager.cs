﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteManager : MonoBehaviour
{

    public Transform launchPos;
    public float xVariation;
    public Transform creationPos;
    public float timeLaunch;


    public GameObject[] SatellitePrefab;
    private SatelliteCache[] SatelliteCaches;
    private float currentTime;


    public static SatelliteManager instance;

    // Use this for initialization
    void Awake()
    {
        instance = this;
        Vector3 creationPosSatellite = creationPos.position;
        SatelliteCaches = new SatelliteCache[SatellitePrefab.Length];

        for (int i = 0; i < SatellitePrefab.Length; i++)
        {
            SatelliteCaches[i] = new SatelliteCache(SatellitePrefab[i], creationPosSatellite, creationPos, 30);

            creationPosSatellite.y += 1;
        }
    }

    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime > timeLaunch)
        {
            SatelliteCaches[Random.Range(0, SatelliteCaches.Length)].GetSatellite().LaunchSatellite(launchPos.position, new Vector2(Random.Range(-5, 5), Random.Range(-5, -1)), 1);
            currentTime -= timeLaunch;
        }
    }

    public void LaunchSatellite(int type, Vector3 SPosition, Vector2 SDirection, float SRotation)
    {
        SatelliteCaches[type].GetSatellite().LaunchSatellite(SPosition, SDirection, SRotation);
    }
}
