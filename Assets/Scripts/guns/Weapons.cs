﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    private Cannon cannon;
    public int maxAmmo;

    public Transform ammoTransfom;
    //TESTING VARIABLES
    public Cartridge[] cartridges;
    public int currentCartridge = 0;
    public GameObject[] bulletsResources;
    public float timecounter = 0;
    public SpriteRenderer sprite;
    private void Start()
    {
        bulletsResources = Resources.LoadAll<GameObject>("Prefabs/Bullets");

        cartridges = new Cartridge[bulletsResources.Length];

        Vector2 spawnPos = ammoTransfom.position;

        for (int i = 0; i < bulletsResources.Length; i++)
        {
            GameObject obj = new GameObject();
            obj.name = "Cartridge_" + i;
            obj.transform.parent = ammoTransfom;
            obj.transform.localPosition = Vector3.zero;

            cartridges[i] = new Cartridge(bulletsResources[i], obj.transform, spawnPos, maxAmmo);
            spawnPos.y -= 1;
        }

        cannon = GetComponentInChildren<Cannon>();
    }

    public void ShotWeapon()
    {
        cannon.ShotCannon(cartridges[currentCartridge]);
        if (currentCartridge != 0)
        {
            timecounter++;
            if (currentCartridge == 1)
            {
                if (timecounter > 300f)
                {
                    Reset();
                    timecounter = 0;
                }
            }
            if (currentCartridge == 4)
            {
                if (timecounter > 250f)
                {
                    Reset();
                    timecounter = 0;
                }
            }
            if (currentCartridge == 3)
            {
                if (timecounter > 150f)
                {
                    Reset();
                    timecounter = 0;
                }
            }
        }
    }
    public void FragmentBullet()
    {
        currentCartridge = 1;
        sprite.sprite = Resources.Load<Sprite>("Azul");
    }
    public void TripleBullet()
    {
        currentCartridge = 4;
        sprite.sprite = Resources.Load<Sprite>("Rojo");
    }
    public void RadioBullet()
    {
        currentCartridge = 3;
        sprite.sprite = Resources.Load<Sprite>("Verde");
    }
    public void Reset()
    {
        sprite.sprite = Resources.Load<Sprite>("Nave_001");
        currentCartridge = 0;
    }
}
