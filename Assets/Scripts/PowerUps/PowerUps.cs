﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerUps : MonoBehaviour {

	protected bool launched;
	protected Vector2 moveSpeed;
	protected float rotationSpeed;
	public ParticleSystem particle;
	public AudioSource audioExplosion;

	private Transform graphics;
	private Collider2D powerupCollider;


	private Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
		graphics = transform.GetChild (0);
		powerupCollider = GetComponent<Collider2D> ();
	}

	public void LaunchPowerUp(Vector3 position,Vector2 direction, float rotation)
    {
		transform.position = position;
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moveSpeed = direction;
		rotationSpeed = rotation;
		graphics.gameObject.SetActive (true);
        powerupCollider.enabled = true;
	}

	// Update is called once per frame
	protected void Update ()
    {
		if (launched) {
			transform.Translate (moveSpeed.x*Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			graphics.Rotate(0,0, rotationSpeed);
		}
	}

	protected void OnTriggerEnter2D(Collider2D other)
    {
		if (other.tag == "Player")
        {
			Explode ();
		}else if(other.tag == "Finish")
        {
			Reset ();
		}
	}

	protected void Reset()
    {
		transform.position = iniPos;
		launched = false;
	}

	protected virtual void Explode()
    {
		audioExplosion.Play ();
        powerupCollider.enabled = false;
		launched = false;
		particle.Play ();
		graphics.gameObject.SetActive (false);
		Invoke ("Reset", 1);
	}
}
