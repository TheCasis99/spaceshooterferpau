﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsCache
{
	private PowerUps[] powerups;
	private int currentPowerUp = 0;


	public PowerUpsCache(GameObject prefabPowerUps, Vector3 position, Transform parent, int numPowerUps){
        powerups = new PowerUps[numPowerUps];

		Vector3 tmpPosition = position;
		for(int i=0;i< numPowerUps; i++){
            powerups[i] = GameObject.Instantiate (prefabPowerUps, tmpPosition, Quaternion.identity, parent).GetComponent<PowerUps>();
            powerups[i].name = prefabPowerUps.name+"_PowerUp_" + i;
			tmpPosition.x += 1;
		}
	}

	public PowerUps GetPowerUp(){
		if (currentPowerUp > powerups.Length - 1) {
            currentPowerUp = 0;
		}

		return powerups[currentPowerUp++];
	}
}
