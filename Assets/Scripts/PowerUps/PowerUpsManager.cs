﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsManager : MonoBehaviour {

	public Transform launchPos;
	public float xVariation;
	public Transform creationPos;
	public float timeLaunch;


	public GameObject[] powerupsPrefabs;
	private PowerUpsCache[] powerupsCaches;
	private float currentTime;
    private Vector3 tmpPos;


	public static PowerUpsManager instance;

	// Use this for initialization
	void Awake () {
		instance = this;
		Vector3 creationPosPowerUps = creationPos.position;
        powerupsCaches = new PowerUpsCache[powerupsPrefabs.Length];

		for (int i = 0; i < powerupsPrefabs.Length; i++) {
            //Big Meteors;
            powerupsCaches[i] = new PowerUpsCache(powerupsPrefabs[i], creationPosPowerUps, creationPos, 30);

            creationPosPowerUps.y += 1;
		}
	}

	void Update(){
		currentTime += Time.deltaTime;
        tmpPos = launchPos.position;
        tmpPos.x = Random.Range(-7, 7);

		if (currentTime > timeLaunch) {
            powerupsCaches[Random.Range(0, powerupsCaches.Length)].GetPowerUp ().LaunchPowerUp (launchPos.position, new Vector2 (Random.Range(-5, 5), Random.Range(-3, -1)),1);
			currentTime -= timeLaunch;
		}
	}

	public void LaunchPowerUps(int type,Vector3 mPosition, Vector2 mDirection, float mRotation){
        powerupsCaches[type].GetPowerUp ().LaunchPowerUp (mPosition, mDirection, mRotation);
	}
}
