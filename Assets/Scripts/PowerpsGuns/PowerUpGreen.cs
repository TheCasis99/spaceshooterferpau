﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpGreen : PowerUps
{
    public GameObject[] customGraphics;
    public Weapons weapon;

    // Use this for initialization
    void Awake()
    {
        weapon = FindObjectOfType<Weapons>();
        int selected = Random.Range(0, customGraphics.Length);
        for (int i = 0; i < customGraphics.Length; i++)
        {
            if (i != selected)
            {
                customGraphics[i].SetActive(false);
            }
        }

        enabled = true;
    }
    protected override void Explode()
    {
        base.Explode();
        weapon.RadioBullet();
    }
}