﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    public void Exit()
    {
        Application.Quit();
    }

    public void Play()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("SpaceShooter");
    }
    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
